import mysql.connector
import os, os.path
import pickle
import socket
import time
import utils
from commands import *
from config import DB_INFORMATIONS
from player import Player
from threading import Thread, Lock
from utils import *
from gamethread import GameThread

mutex = Lock()

class ClientThread(Thread):
    def __init__(self, sock_client):
        Thread.__init__(self)
        self.sock_client = sock_client

    def run(self):
        while True:
            # On récupère la commande
            command = self.sock_client.recv(1).decode('UTF-8')

            # On lance une action en fonction de la commande
            if command == CMD_INSCRIPTION:
                self.inscription()
            elif command == CMD_CONNECTION:
                self.connection()
            elif command == CMD_GET_GAMES:
                self.get_games()
            elif command == CMD_CREATE_GAME:
                self.create_game()
            elif command == CMD_GET_SCORES:
                self.get_scores()
            elif command == CMD_GET_PLAYER_IN_GAME:
                self.get_player_in_game()
            elif command == CMD_JOIN_GAME:
                self.join_game()
            elif command == CMD_LAUNCH_GAME:
                self.launch_game()
            elif command == CMD_STOCK_SOCK_GAME:
                self.stock_sock_game()
                while True:
                    time.sleep(0.5)
            elif command == CMD_LEAVE_GAME:
                self.leave_game()
            elif command == CMD_EDIT_GAME:
                self.edit_game()
            elif command == CMD_GET_LIST_OF_MAPS:
                self.get_list_of_maps()

    def inscription(self):
        print("   <---> Inscription en cours <--->")

        # On récupère les infos pour l'inscription
        data = self.sock_client.recv(4096)
        infos = pickle.loads(data)

        print("      - " + infos[0] + ", " + infos[1])

        db = mysql.connector.connect(**DB_INFORMATIONS)
        cursor = db.cursor()
        query = 'INSERT INTO player (login, password, score, isAdmin) VALUES (%s, %s, 0, 0)'
        cursor.execute(query, infos)
        db.commit()
        cursor.close()
        db.close()

    def connection(self):
        print("   <---> Connexion en cours <--->")

        # On récupère les infos pour la connexion
        data = self.sock_client.recv(8192)
        infos = pickle.loads(data)

        db = mysql.connector.connect(**DB_INFORMATIONS)
        cursor = db.cursor()
        query = 'SELECT * FROM player WHERE login = %s AND password = %s'
        cursor.execute(query, infos)

        selected_player = cursor.fetchone()

        if selected_player:
            already_connected = False
            for player in Global.players:
                if player.login == infos[0]:
                    already_connected = True

            if already_connected:
                print("      - DEJA CONNECTE : " + infos[0] + ", " + infos[1])
                self.sock_client.send(CMD_ACTION_FAILED.encode('UTF-8'))
            else:
                print("      - SUCCES : " + infos[0] + ", " + infos[1])
                Global.players.append(Player(infos[0], selected_player[3], selected_player[4]))
                self.sock_client.send(CMD_ACTION_SUCCESSFUL.encode('UTF-8'))
                self.sock_client.send(pickle.dumps([selected_player[3], selected_player[4]]))
        else:
            print("      - ECHEC : " + infos[0] + ", " + infos[1])
            self.sock_client.send(CMD_ACTION_FAILED.encode('UTF-8'))

        cursor.close()
        db.close()

    def get_games(self):
        print("   <---> Demande des parties en cours <--->")

        # On envoie la liste des parties
        self.sock_client.send(pickle.dumps(Global.games))

    def create_game(self):
        print("   <---> Création d'une partie en cours <--->")

        # On récupère les infos de la partie
        data = self.sock_client.recv(8192)
        game = pickle.loads(data)

        print("     - Créateur : " + game.players[0].login)

        Global.games.append(game)

    def get_player_in_game(self):
        print("   <---> Demande des participants à une partie <--->")

        # On récupère le joueur
        data = self.sock_client.recv(8192)
        current_player = pickle.loads(data)

        current_game = None
        for game in Global.games:
            for player in game.players:
                if player.login == current_player.login:
                    current_game = game
                    break

        if current_game == None:
            self.sock_client.send(CMD_ACTION_FAILED.encode('UTF-8'))
        else:
            self.sock_client.send(CMD_ACTION_SUCCESSFUL.encode('UTF-8'))
            self.sock_client.send(pickle.dumps(game))

    def get_scores(self):
        print("   <---> Demande des scores en cours <--->")

        db = mysql.connector.connect(**DB_INFORMATIONS)
        cursor = db.cursor()
        query = 'SELECT login, score FROM player ORDER BY score DESC'
        cursor.execute(query)

        scores = []

        for (login, score) in cursor:
            scores.append([login, score])

        # On envoie les scores
        self.sock_client.send(pickle.dumps(scores))

        cursor.close()
        db.close()

    def join_game(self):
        print("   <---> Quelqu'un essaie de rejoindre une partie <--->")

        # On récupère les infos de la partie
        data = self.sock_client.recv(8192)
        infos = pickle.loads(data)

        password = Global.games[infos[0]].password
        current_game = Global.games[infos[0]]

        self.sock_client.send(pickle.dumps(password))

        if password == '':
            if len(current_game.players) <  int(current_game.nb_max_players):
                Global.games[infos[0]].players.append(infos[1])
                #self.sock_client.send(CMD_ACTION_SUCCESSFUL.encode('UTF-8'))
            else:
                self.sock_client.send(CMD_ACTION_FAILED.encode('UTF-8'))
        else:
            command = self.sock_client.recv(1).decode('UTF-8')
            if command == CMD_ACTION_SUCCESSFUL:
                Global.games[infos[0]].players.append(infos[1])

    def launch_game(self):
        print("   <---> Lancement d'une partie <--->")

        data = self.sock_client.recv(8192)
        login = pickle.loads(data)

        players = []
        current_game = None
        id = 1

        for game in Global.games:
            if game.creator.login == login:
                for player in Global.players:
                    for other_player in game.players:
                        if player.login == other_player.login:
                            players.append(player)
                            current_game = game
                            player.sock_game.send(CMD_LAUNCH_GAME.encode('UTF-8'))

                            player.sock_game.send(pickle.dumps(id))

                            id = id + 1

                            file = open('map/map' + game.map + '.txt', 'r')
                            nb_tiles_x = int(file.readline())
                            nb_tiles_y = int(file.readline())
                            map = []
                            for i in range(nb_tiles_y):
                                line = file.readline().rstrip()
                                map.append([])
                                for j in line.split(' '):
                                    map[i].append(int(j))

                            file.close()

                            player.sock_game.send(pickle.dumps(map))


        gameThread = GameThread(game, players)
        gameThread.start()

        while True:
            time.sleep(0.5)

    def stock_sock_game(self):
        data = self.sock_client.recv(8192)
        login = pickle.loads(data)

        for player in Global.players:
            if player.login == login:
                player.sock_game = self.sock_client
                break

    def leave_game(self):
        print("   <---> Un joueur quitte une partie <--->")

        data = self.sock_client.recv(8192)
        login = pickle.loads(data)

        for game in Global.games:
            for player in game.players:
                if player.login == login:
                    # Si c'est le créateur on enlève tout le monde et on supprime la partie
                    if game.creator.login == login:
                        del game.players[:]
                        Global.games.remove(game)
                        break
                    else:
                        game.players.remove(player)
                        break

    def edit_game(self):
        print("   <---> Edition d'une partie <--->")

        data = self.sock_client.recv(8192)
        current_game = pickle.loads(data)

        for game in Global.games:
            if game.creator.login == current_game.creator.login:
                game.name = current_game.name
                game.nb_max_players = current_game.nb_max_players
                game.type = current_game.type
                game.map = current_game.map
                game.password = current_game.password

    def get_list_of_maps(self):
        DIR = 'map/'
        nb_of_maps = len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))])

        self.sock_client.send(pickle.dumps(nb_of_maps))