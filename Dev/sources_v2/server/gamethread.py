import mysql.connector
import os, os.path
import pickle
import socket
import utils
import random
import time
from commands import *
from config import DB_INFORMATIONS
from player import Player
from threading import Thread, Lock
from utils import *

class GameThread(Thread):
    def __init__(self, game, players):
        Thread.__init__(self)
        self.game = game
        self.players = players

    def run(self):

        colors = []
        id = 1
        for player in self.players:
            colors.append((id * random.randint(0, 255) % 255, id * random.randint(0, 255) % 255, id * random.randint(0, 255) % 255))
            id = id + 1

        for player in self.players:
            player.sock_game.send(pickle.dumps(colors))

        while True:

            players_grid = []

            for player in self.players:
                data = player.sock_game.recv(82900)
                player_grid = pickle.loads(data)

                players_grid.append(player_grid)

            send_grid = players_grid[0]
            for grid in players_grid:
                for i in range(96):
                    for j in range(96):
                        if grid[i][j] != 0 and send_grid[i][j] == 0:
                            send_grid[i][j] = grid[i][j]

            for player in self.players:
                player.sock_game.send(pickle.dumps(send_grid))