import os, os.path
import socket
import pickle
from threading import Thread
from clientthread import ClientThread
from commands import *
from utils import *

# On crée un socket TCP et on le met à l'écoute
sock_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('0.0.0.0', 9999)
sock_server.bind(server_address)
sock_server.listen(50)

print("<===> Serveur lancé <===>")

Global.players = []

while True:
    # On attend la connexion d'un client
    sock_client, client_address = sock_server.accept()

    print('   <---> Client se connecte au serveur <--->')

    # Lors d'une connexion, on lance un thread par client
    thread_client = ClientThread(sock_client)
    thread_client.start()
