import sys
import socket
import pickle
import utils
from commands import *
from displaygames import DisplayGamesWindow
from inscription import InscriptionWindow
from gamethread import GameThread
from player import Player
from utils import *
from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtGui import *

class ConnectionWindow(QtGui.QMainWindow, uic.loadUiType("ui/connection.ui")[0]):
    def __init__(self, parent = None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        # On cache le texte des textbox mdp
        self.edtPassword.setEchoMode(QLineEdit.Password)

        # Bind les boutons
        self.btnLeave.clicked.connect(self.btnLeaveClicked)
        self.btnInscription.clicked.connect(self.btnInscriptionClicked)
        self.btnConnection.clicked.connect(self.btnConnectionClicked)

    def btnLeaveClicked(self):
        sys.exit()

    def btnInscriptionClicked(self):
        inscWindow = InscriptionWindow(self)
        inscWindow.show()

    def btnConnectionClicked(self):
        print("Connexion en cours ...")

        username = self.edtUsername.text()
        password = self.edtPassword.text()

        # On envoie la commande de connexion
        Global.sock_server.send(CMD_CONNECTION.encode('UTF-8'))

        # On envoie les données
        Global.sock_server.send(pickle.dumps((username, password)))

        response = Global.sock_server.recv(1).decode('UTF-8')

        if response == CMD_ACTION_SUCCESSFUL:
            # On récupère le score et si l'utilisateur est admin ou non
            data = Global.sock_server.recv(8192)
            player_infos = pickle.loads(data)

            # On lance le socket pour le jeu (utilisé plus tard)
            Global.sock_game = utils.server_connection()
            Global.sock_game.send(CMD_STOCK_SOCK_GAME.encode('UTF-8'))
            Global.sock_game.send(pickle.dumps(username))
            thread = GameThread()
            thread.start()

            Global.player = Player(username, player_infos[0], player_infos[1])

            # On affiche la fenêtre suivante
            displayGamesWindow = DisplayGamesWindow(self)
            Global.display_games_window = displayGamesWindow
            displayGamesWindow.show()
            self.hide()
        else:
            QMessageBox.about(self, 'Connexion', 'Connexion échouée ou déjà connecté')
