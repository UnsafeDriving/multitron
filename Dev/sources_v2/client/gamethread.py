import pickle
import socket
import utils
import sys
import math
from commands import *
from player import Player
from threading import Thread
from utils import *
import pygame
from pygame.locals import *
import random
from PyQt4.QtGui import QTableWidgetItem, QMessageBox, QHeaderView

class GameThread(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        while True:
            command = Global.sock_game.recv(1).decode('UTF-8')

            if command == CMD_LAUNCH_GAME:
                data = Global.sock_game.recv(8192)
                id = pickle.loads(data)

                data = Global.sock_game.recv(81920)
                map = pickle.loads(data)

                data = Global.sock_game.recv(81920)
                colors = pickle.loads(data)

                screen = pygame.display.set_mode((480, 480))
                pygame.display.set_caption("Multitron")

                background = pygame.Surface(screen.get_size())
                background = background.convert()
                background.fill((10, 198, 198))

                wallImage = pygame.image.load("images/wall.png").convert()

                DOWN = 1
                RIGHT = 2
                LEFT = 3
                UP = 4

                PLAYER_SIZE = 5
                WALL_SIZE = 30

                clock = pygame.time.Clock()

                playersGrid = []
                for i in range(96):
                    playersGrid.append([])
                    for j in range(96):
                        playersGrid[i].append(0)

                playerImage = pygame.image.load("images/player.png").convert()
                x = random.randint(20, 60) * 5
                y = random.randint(20, 60) * 5

                collideWallId = 1
                collideTailId = 1
                direction = DOWN

                random.seed(1)

                pygame.font.init()
                myfont = pygame.font.Font(None, 50)
                lose = False

                while 1:
                    clock.tick(20)

                    tmpX = math.floor(x / PLAYER_SIZE)
                    tmpY = math.floor(y / PLAYER_SIZE)
                    if not lose:
                        playersGrid[tmpX][tmpY] = id

                        if direction == DOWN:
                            y += PLAYER_SIZE
                        elif direction == RIGHT:
                            x += PLAYER_SIZE
                        elif direction == LEFT:
                            x -= PLAYER_SIZE
                        elif direction == UP:
                            y -= PLAYER_SIZE

                        # Check collision avec les murs
                        tmpY = math.floor(y / WALL_SIZE)
                        tmpX = math.floor(x / WALL_SIZE)
                        if map[tmpY][tmpX] == 1:
                            lose = True
                            collideWallId += 1

                        # Check collision avec la queue
                        tmpY = math.floor(y / PLAYER_SIZE)
                        tmpX = math.floor(x / PLAYER_SIZE)
                        if playersGrid[tmpX][tmpY] != 0:
                            lose = True
                            collideTailId += 1

                        for event in pygame.event.get():
                            if event.type == QUIT:
                                pygame.quit()
                            if event.type == KEYDOWN:
                                if event.key == K_RIGHT:
                                    direction = RIGHT
                                if event.key == K_LEFT:
                                    direction = LEFT
                                if event.key == K_UP:
                                    direction = UP
                                if event.key == K_DOWN:
                                    direction = DOWN

                        if not lose:
                            screen.blit(background, (0, 0))

                            # On dessine les murs
                            for i in range(len(map)):
                                for j in range(len(map[i])):
                                    if map[i][j] == 1:
                                        screen.blit(wallImage, (j * WALL_SIZE, i * WALL_SIZE))

                            screen.blit(playerImage, (x, y))

                    # On dessine une trace des joueurs
                    for i in range(len(playersGrid)):
                        for j in range(len(playersGrid[i])):
                            if playersGrid[i][j] != 0:
                                pygame.draw.rect(screen, colors[playersGrid[i][j] - 1], (i * PLAYER_SIZE, j * PLAYER_SIZE, PLAYER_SIZE, PLAYER_SIZE))

                    if lose:
                        label = myfont.render("Vous avez perdu !", 1, (255,0,0))
                        screen.blit(label, (100, 100))

                    pygame.display.flip()

                    Global.sock_game.send(pickle.dumps(playersGrid))

                    data = Global.sock_game.recv(81920)
                    playersGrid = pickle.loads(data)