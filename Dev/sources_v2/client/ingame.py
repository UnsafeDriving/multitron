import sys
import socket
import pickle
import time
import utils
import creategame
from commands import *
from player import Player
from utils import *
from PyQt4 import  QtGui, uic
from PyQt4.QtGui import QMessageBox

class InGameWindow(QtGui.QMainWindow, uic.loadUiType("ui/ingame.ui")[0]):
    def __init__(self, parent = None, is_creator = False, display_games_window = None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        # Bind les boutons
        self.btnLeave.clicked.connect(self.btnLeave_clicked)
        self.btnRefresh.clicked.connect(self.btnRefresh_clicked)
        self.btnLaunch.clicked.connect(self.btnLaunch_clicked)

        self.display_games_window = display_games_window

        # On cache les boutons si ce n'est pas le créateur
        if not is_creator:
            self.btnLaunch.hide()
            self.btnEdit.hide()

            parent.btnJoinGame.setEnabled(False)
            parent.btnCreateGame.setEnabled(False)

            self.display_games_window = parent
        else:
            self.btnEdit.clicked.connect(self.btnEdit_clicked)

        time.sleep(0.05)

        self.refresh()

    def btnLeave_clicked(self):
        # On quitte la partie, on averti le serveur
        Global.sock_server.send(CMD_LEAVE_GAME.encode('UTF-8'))
        Global.sock_server.send(pickle.dumps(Global.player.login))

        self.display_games_window.btnJoinGame.setEnabled(True)
        self.display_games_window.btnCreateGame.setEnabled(True)

        self.hide()

    def btnLaunch_clicked(self):
        Global.sock_server.send(CMD_LAUNCH_GAME.encode('UTF-8'))
        Global.sock_server.send(pickle.dumps(Global.player.login))

    def btnRefresh_clicked(self):
        self.refresh()

    def btnEdit_clicked(self):
        create_game_window = creategame.CreateGameWindow(self, self.game)
        create_game_window.show()

    def refresh(self):
        Global.sock_server.send(CMD_GET_PLAYER_IN_GAME.encode('UTF-8'))

        Global.sock_server.send(pickle.dumps(Global.player))

        command = Global.sock_server.recv(1).decode('UTF-8')

        if command == CMD_ACTION_SUCCESSFUL:
            data = Global.sock_server.recv(8192)
            game = pickle.loads(data)

            self.game = game

            self.lblGameName.setText(game.name)

            self.tblPlayers.clear()
            for player in game.players:
                self.tblPlayers.addItem(player.login)
        else:
            QMessageBox.about(self, 'Erreur', "La partie n'existe plus ?")
