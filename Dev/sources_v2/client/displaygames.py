import sys
import socket
import pickle
import time
import utils
from commands import *
from utils import *
from creategame import CreateGameWindow
from ingame import InGameWindow
from player import Player
from scores import ScoresWindow
from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtGui import QTableWidgetItem, QMessageBox, QHeaderView

class DisplayGamesWindow(QtGui.QMainWindow, uic.loadUiType("ui/displaygames.ui")[0]):
    def __init__(self, parent = None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        # Bind les boutons
        self.btnLeave.clicked.connect(self.btnLeave_clicked)
        self.btnRefresh.clicked.connect(self.btnRefresh_clicked)
        self.btnCreateGame.clicked.connect(self.btnCreateGame_clicked)
        self.btnDisplayScores.clicked.connect(self.btnDisplayScores_clicked)
        self.btnJoinGame.clicked.connect(self.btnJoinGame_clicked)

        time.sleep(0.05)

        # On affiche les parties à l'ouverture
        self.refresh()

    def btnLeave_clicked(self):
        Global.sock_game.shutdown(socket.SHUT_RDWR)
        sys.exit()

    def btnCreateGame_clicked(self):
        create_game_window = CreateGameWindow(self)
        create_game_window.show()

    def btnDisplayScores_clicked(self):
        scores_window = ScoresWindow(self)
        scores_window.show()

    def btnRefresh_clicked(self):
        self.refresh()

    def btnJoinGame_clicked(self):
        row = self.tblGames.currentItem().row()

        Global.sock_server.send(CMD_JOIN_GAME.encode('UTF-8'))

        Global.sock_server.send(pickle.dumps([row, Global.player]))

        data = Global.sock_server.recv(8192)
        password = pickle.loads(data)

        if password == '':
            in_game_window = InGameWindow(self)
            in_game_window.show()
        else:
            text, ok = QtGui.QInputDialog.getText(self, 'Mot de passe',
            'Entrez le mot de passe de la partie :')

            if ok:
                if str(text) == password:
                    Global.sock_server.send(CMD_ACTION_SUCCESSFUL.encode('UTF-8'))
                    in_game_window = InGameWindow(self)
                    in_game_window.show()
                else:
                    Global.sock_server.send(CMD_ACTION_FAILED.encode('UTF-8'))
                    QMessageBox.about(self, 'Erreur', 'Impossible de rejoindre la partie (mauvais mot de passe ?)')


    def refresh(self):
        # On demande à avoir la liste des parties
        Global.sock_server.send(CMD_GET_GAMES.encode('UTF-8'))

        # On récupère la liste des parties
        data = Global.sock_server.recv(8192)
        games = pickle.loads(data)

        self.lblAvailablesGames.setText("Parties disponibles - " + Global.player.login)

        # On vide la tableWidget
        self.tblGames.clear()

        # On set les colonnes dans la tableWidget
        header_labels = ('Nom', 'Nombre de joueurs', 'Type', 'Carte', 'En cours ?', 'Mot de passe ?')
        self.tblGames.setColumnCount(6)
        self.tblGames.setHorizontalHeaderLabels(header_labels)
        self.tblGames.horizontalHeader().setResizeMode(QHeaderView.Stretch)

        # On les affiche dans la tableWidget
        self.tblGames.setRowCount(len(games))
        row = 0
        for game in games:
            self.tblGames.setItem(row, 0, QTableWidgetItem(game.name))
            self.tblGames.setItem(row, 1, QTableWidgetItem(str(len(game.players)) + "/" + str(game.nb_max_players)))
            self.tblGames.setItem(row, 2, QTableWidgetItem(game.type))
            self.tblGames.setItem(row, 3, QTableWidgetItem(str(game.map)))
            self.tblGames.setItem(row, 4, QTableWidgetItem('Non'))
            self.tblGames.setItem(row, 5, QTableWidgetItem('Non' if game.password == '' else 'Oui'))
            row = row + 1