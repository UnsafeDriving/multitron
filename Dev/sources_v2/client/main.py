import sys
import utils
from utils import *
from PyQt4 import QtGui
from connection import ConnectionWindow

# On se connecte au serveur au lancement de l'application
Global.sock_server = utils.server_connection()

app = QtGui.QApplication(sys.argv)
connWindow = ConnectionWindow(None)
connWindow.show()
app.exec_()