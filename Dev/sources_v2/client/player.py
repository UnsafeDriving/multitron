import socket

class Player:

    def __init__(self, login, score, is_admin = False):
        self.login = login
        self.score = score
        self.is_admin = is_admin
        self.sock_game = None

    def show(self):
        print(self.login + ", " + str(self.score) + ", " + str(self.is_admin))