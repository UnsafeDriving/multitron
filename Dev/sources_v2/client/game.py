class Game:
    def __init__(self, creator, name, nb_max_players, type, map, password = ''):
        self.creator = creator
        self.name = name
        self.nb_max_players = nb_max_players
        self.type = type
        self.map = map
        self.password = password
        self.players = []
        self.players.append(creator)