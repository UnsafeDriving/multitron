import socket

server_ip = 'SBDWIS-T430'
server_port = 9999

def server_connection():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (server_ip, server_port)
    sock.connect(server_address)

    return sock

def server_udp_connection():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_address = (server_ip, server_port)
    sock.connect(server_address)

    return sock


class Global:
    player = None
    sock_server = None
    sock_game = None
    display_games_window = None
