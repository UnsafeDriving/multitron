import sys
import socket
import pickle
import time
import utils
from commands import *
from game import Game
from ingame import InGameWindow
from player import Player
from utils import *
from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtGui import QTableWidgetItem, QMessageBox, QHeaderView

class CreateGameWindow(QtGui.QMainWindow, uic.loadUiType("ui/editgame.ui")[0]):
    def __init__(self, parent = None, game = None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        # Rempli les comboBox
        types = ['FFA', 'Team']
        self.cbxGameType.addItems(types)

        Global.sock_server.send(CMD_GET_LIST_OF_MAPS.encode('UTF-8'))

        data = Global.sock_server.recv(8192)
        infos = pickle.loads(data)

        maps = []
        for i in range(infos):
            maps.append(str(i + 1))

        self.cbxGameMap.addItems(maps)

        self.parent = parent
        self.game = game

        if game == None:
            parent.btnJoinGame.setEnabled(False)
            parent.btnCreateGame.setEnabled(False)
            self.btnCreateGame.clicked.connect(self.btnCreateGame_clicked)
        else:
            self.btnCreateGame.setText("Modifier")
            self.btnCreateGame.clicked.connect(self.btnEditGame_clicked)
            self.edtGameName.setText(self.game.name)
            self.sbxNbMaxPlayers.setValue(int(self.game.nb_max_players))
            type_index = self.cbxGameType.findText(self.game.type, QtCore.Qt.MatchFixedString)
            self.cbxGameType.setCurrentIndex(type_index)
            map_index = self.cbxGameMap.findText(self.game.map, QtCore.Qt.MatchFixedString)
            self.cbxGameMap.setCurrentIndex(map_index)
            self.edtGamePassword.setText(self.game.password)

        # Bind les boutons
        self.btnCancel.clicked.connect(self.btnCancel_clicked)

    def btnCancel_clicked(self):
        if self.game == None:
            self.parent.btnJoinGame.setEnabled(True)
            self.parent.btnCreateGame.setEnabled(True)

        self.close()

    def btnEditGame_clicked(self):
        Global.sock_server.send(CMD_EDIT_GAME.encode('UTF-8'))

        self.game.name = self.edtGameName.text()
        self.game.nb_max_players = self.sbxNbMaxPlayers.text()
        self.game.type = self.cbxGameType.currentText()
        self.game.map = self.cbxGameMap.currentText()
        self.game.password = self.edtGamePassword.text()

        Global.sock_server.send(pickle.dumps(self.game))

        self.close()

    def btnCreateGame_clicked(self):

        # On demande la création d'une partie
        Global.sock_server.send(CMD_CREATE_GAME.encode('UTF-8'))

        game = Game(Global.player,
                    self.edtGameName.text(),
                    self.sbxNbMaxPlayers.text(),
                    self.cbxGameType.currentText(),
                    self.cbxGameMap.currentText(),
                    self.edtGamePassword.text())

        Global.sock_server.send(pickle.dumps(game))

        time.sleep(0.05)
        self.parent.refresh()
        time.sleep(0.05)

        in_game_window = InGameWindow(self, True, self.parent)
        in_game_window.show()

        self.close()
