import sys
import socket
import pickle
import utils
from commands import *
from utils import *
from creategame import CreateGameWindow
from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtGui import QTableWidgetItem, QMessageBox, QHeaderView

class ScoresWindow(QtGui.QMainWindow, uic.loadUiType("ui/scores.ui")[0]):
    def __init__(self, parent = None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        # On set les colonnes dans la tableWidget
        header_labels = ('Pseudo', 'Score')
        self.tblScores.setColumnCount(2)
        self.tblScores.setHorizontalHeaderLabels(header_labels)
        self.tblScores.horizontalHeader().setResizeMode(QHeaderView.Stretch)

        # Bind les boutons
        self.btnLeave.clicked.connect(self.btnLeave_clicked)
        self.btnEdit.hide()
        self.btnDelete.hide()

        # On demande à avoir la liste des parties
        Global.sock_server.send(CMD_GET_SCORES.encode('UTF-8'))

        # On récupère les scores
        data = Global.sock_server.recv(8192)
        scores = pickle.loads(data)

        self.tblScores.setRowCount(len(scores))
        row = 0
        for (login, score) in scores:
            self.tblScores.setItem(row, 0, QTableWidgetItem(login))
            self.tblScores.setItem(row, 1, QTableWidgetItem(str(score)))
            row = row + 1

    def btnLeave_clicked(self):
        self.close()