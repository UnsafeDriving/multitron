import sys
import socket
import pickle
import utils
from commands import *
from utils import *
from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtGui import QLineEdit, QMessageBox

class InscriptionWindow(QtGui.QMainWindow, uic.loadUiType("ui/inscription.ui")[0]):
    def __init__(self, parent = None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        # On cache le texte des textbox mdp
        self.edtPassword.setEchoMode(QLineEdit.Password)
        self.edtConfirmPassword.setEchoMode(QLineEdit.Password)

        # Bind les boutons
        self.btnCancel.clicked.connect(self.btnCancelClicked)
        self.btnInscription.clicked.connect(self.btnInscriptionClicked)

    def btnCancelClicked(self):
        self.close()

    def btnInscriptionClicked(self):
        username = self.edtUsername.text()
        password = self.edtPassword.text()
        confirmPassword = self.edtConfirmPassword.text()

        if password != confirmPassword:
            print("Les mots de passe sont différents !")
        else:
            self.inscription(username, password)

    def inscription(self, username, password):
        print("Inscription en cours ...")


        # On envoie la commande d'inscription
        Global.sock_server.send(CMD_INSCRIPTION.encode('UTF-8'))
        # On envoie les données
        Global.sock_server.send(pickle.dumps([username, password]))

        QMessageBox.about(self, 'Inscription', 'Inscription réussie !')
